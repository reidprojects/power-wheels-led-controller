#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#define LED_PIN    6
#define LED_COUNT 8

#define WHITE_MODE_PIN 2
#define POLICE_MODE_PIN 3
#define TRASH_MODE_PIN 4
#define CONSTRUCTION_MODE_PIN 5

Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRB + NEO_KHZ800);
enum lights_mode {
  OFF,
  POLICE,
  TRASH,
  CONSTRUCTION,
  WHITE,
  POLICE_WITH_WHITE,
  TRASH_WITH_WHITE,
  CONSTRUCTION_WITH_WHITE,
};


lights_mode current_mode = OFF;


void setup() {
  strip.begin();
  strip.show();
  strip.setBrightness(255);
  
  pinMode(WHITE_MODE_PIN, INPUT);
  pinMode(POLICE_MODE_PIN, INPUT);
  pinMode(TRASH_MODE_PIN, INPUT);
  pinMode(CONSTRUCTION_MODE_PIN, INPUT);
}

void loop()
{
    check_mode();

    switch (current_mode)
    {
      case OFF:
        color_strip(strip.Color(0, 0, 0));
        break;
      case POLICE:
        flash_lights(false, strip.Color(255,0,0), strip.Color(0,0,255));
        break;
      case TRASH:
        flash_lights(false, strip.Color(0,   255,   0), strip.Color(0,   255,   0));
        break;
      case CONSTRUCTION:
        flash_lights(false, strip.Color(255,165,0), strip.Color(255,165,0));
        break;
      case POLICE_WITH_WHITE:
        flash_lights(true, strip.Color(255,0,0), strip.Color(0,0,255));
        break;
      case TRASH_WITH_WHITE:
        flash_lights(true, strip.Color(0,   255,   0), strip.Color(0,   255,   0));
        break;
      case CONSTRUCTION_WITH_WHITE:
        flash_lights(true, strip.Color(255,165,0), strip.Color(255,165,0));
        break;
    }
}

void check_mode()
{
  int white_button = digitalRead(WHITE_MODE_PIN);
  int police_button = digitalRead(POLICE_MODE_PIN);
  int trash_button = digitalRead(TRASH_MODE_PIN);
  int construction_button = digitalRead(CONSTRUCTION_MODE_PIN);

  if(white_button == HIGH)
  {
    if(current_mode == WHITE)
    {
      current_mode = OFF;
    }
    else
    {
      current_mode = WHITE;
    }
  }

  if(police_button == HIGH)
  {
    if(current_mode == WHITE)
    {
      current_mode = POLICE_WITH_WHITE;
    }
    else if(current_mode == POLICE)
    {
      current_mode = OFF;
    }
    else
    {
      current_mode = POLICE;
    }
  }

  if(trash_button == HIGH)
  {
    if(current_mode == WHITE)
    {
      current_mode = TRASH_WITH_WHITE;
    }
    else if(current_mode == TRASH)
    {
      current_mode = OFF;
    }
    else
    {
      current_mode = TRASH;
    }
  }

  if(construction_button == HIGH)
  {
    if(current_mode == WHITE)
    {
      current_mode = CONSTRUCTION_WITH_WHITE;
    }
    else if(current_mode == CONSTRUCTION)
    {
      current_mode = OFF;
    }
    else
    {
      current_mode = CONSTRUCTION;
    }
  }
}

void flash_lights(bool has_white_lights, uint32_t color_a, uint32_t color_b)
{
    int wait = 80;
    if(has_white_lights)
    {
      strip.setPixelColor(3, strip.Color(255,   255,   255)); 
      strip.setPixelColor(4, strip.Color(255,   255,   255)); 
    }
    
    strip.setPixelColor(0, color_a);
    strip.setPixelColor(2, color_a);
    if(!has_white_lights)
    {
      
      strip.setPixelColor(3, color_a); 
    }
    strip.setPixelColor(6, color_a);         
    strip.show();                          
    delay(wait);  
    strip.setPixelColor(0, strip.Color(0,   0,   0));
    strip.setPixelColor(2, strip.Color(0,   0,   0));
    if(!has_white_lights)
    {
      
      strip.setPixelColor(3, strip.Color(0,   0,   0)); 
    }
    strip.setPixelColor(6, strip.Color(0,   0,   0));           
    strip.show();                          
    delay(wait);  
    strip.setPixelColor(0, color_a);
    strip.setPixelColor(2, color_a);
    if(!has_white_lights)
    {
      
      strip.setPixelColor(3, color_a); 
    }
    strip.setPixelColor(6, color_a);           
    strip.show();                          
    delay(wait);  
    strip.setPixelColor(0, strip.Color(0,   0,   0));
    strip.setPixelColor(2, strip.Color(0,   0,   0));
    if(!has_white_lights)
    {
      
      strip.setPixelColor(3, strip.Color(0,   0,   0)); 
    } 
    strip.setPixelColor(6, strip.Color(0,   0,   0));          
    strip.show();                          
    delay(wait);  





    strip.setPixelColor(1, color_b);
    if(!has_white_lights)
    {
      strip.setPixelColor(4, color_b);
      
    }
    strip.setPixelColor(5, color_b);
    strip.setPixelColor(7, color_b);         
    strip.show();                          
    delay(wait);  
    strip.setPixelColor(1, strip.Color(0,   0,   0));  
    if(!has_white_lights)
    {
      strip.setPixelColor(4, strip.Color(0,   0,   0));
      
    }
    strip.setPixelColor(5, strip.Color(0,   0,   0));
    strip.setPixelColor(7, strip.Color(0,   0,   0));          
    strip.show();                          
    delay(wait);  
    strip.setPixelColor(1, color_b); 
    if(!has_white_lights)
    {
      strip.setPixelColor(4, color_b);
      
    }
    strip.setPixelColor(5, color_b);
    strip.setPixelColor(7, color_b);           
    strip.show();                          
    delay(wait);  
    strip.setPixelColor(1, strip.Color(0,   0,   0)); 
    if(!has_white_lights)
    {
      strip.setPixelColor(4, strip.Color(0,   0,   0));
      
    }
    strip.setPixelColor(5, strip.Color(0,   0,   0));
    strip.setPixelColor(7, strip.Color(0,   0,   0));           
    strip.show();                          
    delay(wait);  
}

void color_strip(uint32_t color)
{
  for(int i=0; i<strip.numPixels(); i++) 
  {
    strip.setPixelColor(i, color);
  }
  strip.show();
}
